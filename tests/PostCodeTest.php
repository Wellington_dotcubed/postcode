<?php

namespace Dotcubed\Tests;

use Dotcubed\PostCode\PostCode;

class PostCodeTest
{

    public function testSearch()
    {
        $number = 'WA1 1RG'; // Valid
        $object = PostCode::config('ak_jtthax8lCSRuTpqUNUZ5vtWn59Sx7');
        $response = $object->search($number);

        $this->assertEquals(200, $response['status']);
        $this->assertNotEmpty($response['data']);
    }

    public function testExceptionGetCompanyByNumber()
    {
        $this->expectException(\Exception::class);

        $number = 'AAAABBBCCC'; // Invalid
        $object = PostCode::config('ak_jtthax8lCSRuTpqUNUZ5vtWn59Sx7');

        $object->search($number);
    }
}